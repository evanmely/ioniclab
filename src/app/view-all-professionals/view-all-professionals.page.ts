import { Component, OnInit } from '@angular/core';
import { professional } from '../models/professionals';
import { environment } from 'src/environments/environment';
import { professionalService } from '../services/professional.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-view-all-professionals',
  templateUrl: './view-all-professionals.page.html',
  styleUrls: ['./view-all-professionals.page.scss'],
})
export class ViewAllProfessionalsPage implements OnInit {
  professionalist: professional [];
  imageUri = environment.server_resources + environment.professional_suffix_avatar;
  previousIsActive: Boolean = false;
  nextIsActive: Boolean = true;
  pageMeta: any; //  Page Metadata
  pageList = [];
  id = 3;
  pageListBeta = Array;
  professionalsAreLoading = false;
  isFilter: Boolean = false;
  constructor(private  hs: professionalService,private navctr:NavController) { 
    
  }

  ngOnInit() {
   this.fetch();
  }

  async fetch() {
    
    this.hs.filterprofessionals(this.id).subscribe(async (res) => {
      
        this.professionalist = this.hs.metaData;
      
     
    }, async (err) => {
      
      console.log('Couldn\'t fetch professional id ' + this.id, 'danger');
    });
}

}
