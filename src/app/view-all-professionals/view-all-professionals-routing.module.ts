import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewAllProfessionalsPage } from './view-all-professionals.page';

const routes: Routes = [
  {
    path: '',
    component: ViewAllProfessionalsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewAllProfessionalsPageRoutingModule {}
