import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewAllProfessionalsPageRoutingModule } from './view-all-professionals-routing.module';

import { ViewAllProfessionalsPage } from './view-all-professionals.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewAllProfessionalsPageRoutingModule
  ],
  declarations: [ViewAllProfessionalsPage]
})
export class ViewAllProfessionalsPageModule {}
