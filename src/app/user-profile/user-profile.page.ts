import { Component, OnInit } from '@angular/core';
import { UserProfileService } from '../services/user_profile.service';
import { NavController } from '@ionic/angular';
import { Profile } from '../models/user_profile';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  profile: Profile;
  image_suffix = environment.server_resources;
  logger: any;
  constructor( private cs: UserProfileService ,public navCtrl:NavController ) 
    {
    this.profile = new Profile();
  }
  async editProfile(){
    this.navCtrl.navigateRoot('/edit-profile');
  }
  ngOnInit() {
    this.cs.getClient(60).subscribe(res => {
      this.profile = res;
      this.image_suffix= environment.server_resources+"/users/avatar/"+this.profile.avatar;
      console.log(this.image_suffix);
    })
  }
}
