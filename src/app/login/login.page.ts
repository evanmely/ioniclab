import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loadingBarColor = 'primary';
  isLoading: Boolean = false;
  msisdn = '';
  login = {
    phone: '',
    password: '',
  }
  returnUrl: string;
  constructor(public navCtrl: NavController, private authService: AuthService) {

  }
  async openRegister() {
    this.navCtrl.navigateRoot('/register');
  }
  async openDashboard() {
    this.navCtrl.navigateRoot('/dashboard');
  }
  ngOnInit() {
  }
  onSubmitLogin() {
    this.isLoading = true;
    this.login.phone = this.formatphoneNo(this.msisdn);
    this.authService.login(this.login.phone, this.login.password).pipe(first()).subscribe(data => {

      console.log('data is ======',data);
      this.isLoading = false;
      if(data.success===true)
{
  console.log('success',data);
  this.navCtrl.navigateRoot('/dashboard').then(() => {
  }, (res) => {
    console.log('refused to redirect: ' + res);
  });
} else{
  console.log('invalid credentials');
}
     
    }, (err) => {
      this.isLoading = false;
      console.error(err);

    });
  }

  // format phone number to 254
  formatphoneNo(data: string) {
    if (data.charAt(0) === '0') {
      const cleanData = data.replace('0', '254');
      console.log('Replaced', cleanData);
      return cleanData;
    }
    return data;
  }
  
}
