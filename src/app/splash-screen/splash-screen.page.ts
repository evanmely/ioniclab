import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.page.html',
  styleUrls: ['./splash-screen.page.scss'],
})
export class SplashScreenPage implements OnInit {

  constructor(public navCtrl:NavController) { }

  ngOnInit() {
  }
  async openTab2(){
    this.navCtrl.navigateRoot('/login');
  }
}
