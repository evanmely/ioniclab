import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MenuController,IonicModule, NavController} from '@ionic/angular';
import { Category } from '../models/category';
import { CategoryService } from '../services/categories.service';
import { professionalService } from '../services/professional.service';
import { professional } from '../models/professionals';





@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
  
})
export class DashboardPage implements OnInit {
  category: Category;
  professionals:professional;
  categoryBuffer: Category;
  categories: Category[] = [];
  id: '';

  constructor(private menu: MenuController,
    private cs: CategoryService, private categoryService: CategoryService,public navCtrl:NavController, private proService: professionalService) 
    {
    this.category = new Category();
    this.categoryBuffer = new Category();

  
  }
  sliderOpts = {
    autoplay: true,
    zoom: {
      maxRatio: 5
    }
  };
 
 openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }
  getCategory(id): void {
    console.log('category id', id);
    this.id=id;
    console.log('categoryid2', this.id);

    this.proService.filterprofessionals(this.id).subscribe(res => {
      console.log('professional search res', res);
     res=this.professionals;
     this.navCtrl.navigateRoot("/view-all-professionals")
    });


    // this.categoryBuffer = this.categories.find((c) => c.id === id);
    // this.navCtrl.navigateRoot('/view-all-professionals');


    // const updateDialogRef = this.dialog.open(UpdateCategoryDialogComponent, {
    //   width : '500px',
    //   data :  this.categoryBuffer
    };
    
  async openProfilePage(){
    this.navCtrl.navigateRoot('/user-profile');
  }

  openEnd() {
    this.menu.open('end');
  }
  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
  ngOnInit() {
    this.cs.getAll().subscribe(res => {
      this.categories = res;
    })
  }

}
