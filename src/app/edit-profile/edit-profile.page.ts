import { Component, OnInit } from '@angular/core';
import { Profile } from '../models/user_profile';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx'
import { File } from '@ionic-native/file/ngx';
import { environment } from 'src/environments/environment';
import { UserProfileService } from '../services/user_profile.service';
import { NavController, ActionSheetController } from '@ionic/angular';
import { async } from '@angular/core/testing';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  update_profile: Profile;
  image_suffix = environment.server_resources;
  logger: any;
  croppedImagepath = "";
  isLoading = false;
 


  imagePickerOptions = {
    maximumImagesCount: 1,
    quality: 50
  };
  // user_data<any>: {
  //   first_name: '',
  //   last_name: '',
  //   email: '',
  //   bio: '',
  //   phone: ''
  // };
  constructor( private cs: UserProfileService ,public navCtrl:NavController, private camera: Camera,
    public actionSheetController: ActionSheetController,
    private file: File ) 
    {
    this.update_profile = new Profile();
  }
  
  ngOnInit() {
    this.cs.getClient(60).subscribe(res => {
      this.update_profile = res;
      this.image_suffix= environment.server_resources+"/users/avatar/"+this.update_profile.avatar;
      console.log(this.image_suffix);
    })
  }
  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
    
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }
  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Gallery',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  async updateUser(){
    this.update_profile.avatar=  'data:image/jpeg;base64,'+this.update_profile.avatar
    this.cs.updateClient(this.update_profile).subscribe(async (res)=> 
    {
      
      console.log('update res', res);
      if(res.success==='true'){
        const message = `profile updated successfully`;
      }
      else{
        const message = ` Failed to update profile`;
      }
    })
  }
}
