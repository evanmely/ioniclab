import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Category } from '../models/category';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  categories: Category[];
  uri = environment.server_url_1;
  suffix = environment.categories_suffix;
  constructor(private http: HttpClient) {
  }
  getAll(): Observable<any> {
    return this.http.get<any>(`${this.uri + this.suffix}?api_token=rbtb2hLsRKmSOoFcBGHA2zRSFP8FN48QX93f4kF171oa8PvPkhiICWUwujq5`).pipe(map((response) => {
      return this.categories = response.data;
    }));
  }
}