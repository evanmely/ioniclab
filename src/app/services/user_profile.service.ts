import { Injectable } from '@angular/core'
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { Profile } from '../models/user_profile';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
  uri = environment.server_url_1;
  post_suffix = environment.professional_suffix_post;
  get_suffix = environment.professional_suffix_get;
  getOne_suffix = environment.professional_suffix_getOne;
  getOneUser_suffix = environment.user_suffix_getOne;
  put_suffix = environment.client_suffix_put;
  search_suffix = environment.professional_filter_suffix;
  forms_suffix = environment.professional_suffix_forms;
  image_suffix = environment.server_resources;
  
  metaData:  any;
  constructor(private http: HttpClient, 
    ) {
  }  
  getClient(id): Observable<any> {
    //?api_token=rbtb2hLsRKmSOoFcBGHA2zRSFP8FN48QX93f4kF171oa8PvPkhiICWUwujq5`
    return this.http.get<any>(this.uri + this.getOneUser_suffix + id + '&api_token=rbtb2hLsRKmSOoFcBGHA2zRSFP8FN48QX93f4kF171oa8PvPkhiICWUwujq5`').pipe(map(res => {
      return res;
    }));
  }
  
  
  updateClient(update_profile) {
    console.log('This is client infor',update_profile);
    //id: 4;
    return this.http.post<any>(`${this.uri + this.put_suffix}?api_token=rbtb2hLsRKmSOoFcBGHA2zRSFP8FN48QX93f4kF171oa8PvPkhiICWUwujq5`,update_profile).map(res => res);
  }
}

