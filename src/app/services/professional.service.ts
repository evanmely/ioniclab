import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, first} from 'rxjs/operators';
import { query } from '@angular/animations';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import {Observable} from 'rxjs';
import { professional } from '../models/professionals';
@Injectable({
  providedIn: 'root'
})
export class professionalService {
  uri = environment.server_url_1;
  post_suffix = environment.professional_suffix_post;
  get_suffix = environment.professional_suffix_get;
  getOne_suffix = environment.professional_suffix_getOne;
  put_suffix = environment.client_suffix_put;
  search_suffix = environment.professional_filter_suffix;
  forms_suffix = environment.professional_suffix_forms;
  metaData:  professional[];
  constructor(private http: HttpClient) {
  }
 
  getprofessionals(page?: Number): Observable<any> {
    const pageNo = page === undefined ? 1 : page;
    console.log('Server 2', environment.server_url_2);
    
    return this.http.get<any>(`${this.uri + this.get_suffix}?api_token=rbtb2hLsRKmSOoFcBGHA2zRSFP8FN48QX93f4kF171oa8PvPkhiICWUwujq5`).pipe(map((res) => {
      this.metaData = res.meta;

      console.log((`${this.uri + this.get_suffix}?api_token=rbtb2hLsRKmSOoFcBGHA2zRSFP8FN48QX93f4kF171oa8PvPkhiICWUwujq5`));
      return res.data;
    }));
  }
  
  getprofessional(id): Observable<any> {
    return this.http.get<any>(this.uri + this.getOne_suffix + id).pipe(map(res => {
      return res;
    }));
  }
 

  filterprofessionals(category): Observable<any> {
    console.log('service category id', category);
    console.log('filter data', category);
    return this.http.get<any>(`${this.uri + this.search_suffix}?api_token=rbtb2hLsRKmSOoFcBGHA2zRSFP8FN48QX93f4kF171oa8PvPkhiICWUwujq5&id=${category}`).pipe(map((res) => {
      this.metaData = res.data;
  }));
}
}