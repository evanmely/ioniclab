import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  public currentTokenSubject: BehaviorSubject<any>;
  public currentToken: Observable<any>;
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('user')));
    this.currentTokenSubject = new BehaviorSubject<String>(localStorage.getItem('token'));
    this.currentUser = this.currentUserSubject.asObservable();
    this.currentToken = this.currentTokenSubject.asObservable();
  }
  public get currentUserValue(): any {
    console.log('current User ', this.currentUserSubject.value);
    return this.currentUserSubject.value;
  }
  public get currentTokenValue(): any {
    console.log('current Token', this.currentTokenSubject.value);
    return this.currentTokenSubject.value;
  }
  login(phone: string, password: string) {
    return this.http.post<any>(`${environment.server_url_1}/api/login`, { phone, password, type: 'client' })
      .pipe(map((user) => {
          // login successful if there's a jwt token in the response
        console.log('user', user);
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', user.token.access_token);
          localStorage.setItem('user', JSON.stringify(user.user));
          this.currentUserSubject.next(user.user);
          this.currentTokenSubject.next(user.token.access_token);
        }
        return user;
      }));
  }
  // register(registrationData: Register): Observable<any> {
  //   return this.http.post<any>(`${environment.server_url_1}/api/register`, registrationData).pipe(map(
  //     (res) => {
  //       return res;
  //     }
  //   ));
  // }
  getRoles(): Observable<any> {
    return this.http.get<any>(`${environment.server_url_1 + environment.roles_suffix}`);
  }
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.currentUserSubject.next(null);
    this.currentTokenSubject.next(null);
    //this.ns.clear();
  }
}