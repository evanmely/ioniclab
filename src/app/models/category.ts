export class Category {
    id: number;
    name: String;
    description: String;
    service_fee: number;
    available_professionals_count: number;
}