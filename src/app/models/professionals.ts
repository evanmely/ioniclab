

export class professional {
    id: bigint;
    category_id: number;
    category_name: string;
    first_name: string;
    last_name: string;
    other_names: string;
    national_id_number: string;
    bio: string;
    passport_photo: string;
    gender: string;
    dob: string;
    phone: string;
    email: string;
    county_id: number;
    county_name: string;
    marital_status: string;
    height: number;
    weight: number;
    services: number[];
    skills: number[];
    trainings: number[];
    type_of_placement: string;
    religion: string;
    medical_report?: string;
    fullsize_photo?: string;
    police_clearance?: string;
    national_id_copy?: string;
    languages: string[];
    status: string;
    biometrics?: any;
}